<?php

class TaskList extends Eloquent {

	protected $table = 'taskLists';

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function tasks()
    {
        return $this->belongsToMany('Task', 'taskLists_tasks', 'tasks_id', 'taskLists_id');
    }

}
