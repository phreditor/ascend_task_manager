<?php

class Task extends Eloquent {

	protected $table = 'tasks';

    public function tasks()
    {
        return $this->belongsToMany('TaskList', 'taskLists_tasks', 'taskLists_id', 'tasks_id');
    }

    public function taskStatus()
    {
        return $this->BelongsTo('TaskStatus', 'status_id', 'id');
    }

}
