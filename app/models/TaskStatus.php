<?php

class TaskStatus extends Eloquent {

	protected $table = 'list_taskStatus';

    public function tasks()
    {
        return $this->hasMany('Task', 'status_id', 'id');
    }

}
