<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Provide some basic info to all views.
View::share('MD5_PAD_PRE',MD5_PAD_PRE);
View::share('MD5_PAD_POST',MD5_PAD_POST);
View::share('dCache',DECACHE);


Route::get('/', function()
{
	return View::make('hello');
});


	Route::get('/login', array( 'as' => 'login', 'uses' => 'UserController@login'));

	Route::get('/password/getRemind', array( 'as' => 'getRemind', 'uses' =>'RemindersController@getRemind'));
	Route::get('/password/reset/{token}', array( 'as' => 'getReset', 'uses' =>'RemindersController@getReset'));

	Route::group(array('before' => 'csrf'), function() {
		Route::post('/login', array( 'as' => 'login', 'uses' => 'UserController@login'));

		Route::post('/password/postRemind', array( 'as' => 'postRemind', 'uses' =>'RemindersController@postRemind'));

		Route::post('/password/postReset', array( 'as' => 'postReset', 'uses' =>'RemindersController@postReset'));
	});


	Route::get('/logout', array( 'as' => 'logout', 'uses' => 'UserController@logout'));

	Route::get('/tasklists', array( 'as' => 'tasklists', 'uses' => 'TaskController@getTaskList'));

	Route::get('/tasklist/edit/{id?}', array( 'as' => 'tasklist_edit', 'uses' => 'TaskController@getTaskListEdit'));

	Route::post('/tasklist/process', array( 'as' => 'tasklist_process', 'uses' => 'TaskController@taskListProcess'));

	Route::get('/tasks/{taskList_id?}', array( 'as' => 'tasks', 'uses' => 'TaskController@getTasks'));

	Route::get('/task/edit/{id?}', array( 'as' => 'task_edit', 'uses' => 'TaskController@getTaskEdit'));

	Route::post('/task/process', array( 'as' => 'task_process', 'uses' => 'TaskController@taskProcess'));
