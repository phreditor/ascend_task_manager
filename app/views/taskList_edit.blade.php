@extends('layouts.master')
@section('js')
{{$js or ''}}
<script src="/global/vendor/jquery.validate.min.js"></script>
<script src="/global/vendor/placeholders.jquery.min.js"></script>
@stop

@section('content')

		<div class="row">
			<div class="column col-wide">
				<div class="page-header">
					<h1>Task List Edit: {{$taskList->id or 'new'}}</h1><br><br>
				</div><!-- END .page-header -->

<a href="/tasklists">Return to Task List</a><br><br>

<form action="/tasklist/process" method="POST">
<input type="hidden" name="id" value="{{$taskList->id or ''}}" />
<label for="name">Name</label>
<input type="text" name="name" value="{{$taskList->name or ''}}"/>
<input type="submit" value="submit" />
</form>



			</div><!-- END .col-wide -->
		</div><!-- END .row -->

<!-- end app/views/taskList.blade.php -->
@stop
