@extends('layouts.master')
@section('js')
{{$js or ''}}
<script src="/global/vendor/jquery.validate.min.js"></script>
<script src="/global/vendor/placeholders.jquery.min.js"></script>
@stop

@section('content')

		<div class="row">
			<div class="column col-wide">
				<div class="page-header">
					<h1>Task List</h1><br><br>
				</div><!-- END .page-header -->
<a href="/task/edit">Add Task</a><br><br>
					<table style="width:800px;">
					@if(isset($tasks) && $tasks->count())
						<thead>
						<tr style="border-bottom:1px solid black; margin-bottom:3px;">
						<td>Action</td>
						<td>Description</td>
						<td>Status</td>
						<td>Created</td>
						</tr>
						</thead>
						<tbody>
						@foreach($tasks as $task)
							<tr>
							<td><a href="/task/edit/{{$task->id}}">Edit</a></td>
							<td>{{$task->description}}</td>
							<td>{{$task->taskStatus->status}}</td>
							<td>{{$task->created_at}}</td>
							</tr>
						@endforeach
						</tbody>
					@else
						No tasks available for this list
					@endif

					</table>




			</div><!-- END .col-wide -->
		</div><!-- END .row -->

<!-- end app/views/taskList.blade.php -->
@stop
