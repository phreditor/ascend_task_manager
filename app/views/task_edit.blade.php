@extends('layouts.master')
@section('js')
{{$js or ''}}
<script src="/global/vendor/jquery.validate.min.js"></script>
<script src="/global/vendor/placeholders.jquery.min.js"></script>
@stop

@section('content')

		<div class="row">
			<div class="column col-wide">
				<div class="page-header">
					<h1>Task Edit: {{$task->id or 'new'}}</h1><br><br>
				</div><!-- END .page-header -->

				<a href="/tasklists">Return to Tasks</a><br><br>

				<form action="/task/process" method="POST">
				<input type="hidden" name="id" value="{{$task->id or ''}}" />
				<label for="description">Description</label>
				<input type="text" name="description" value="{{$task->description or ''}}"/>
				{{ Form::select('status_id', $taskStatuses, 1)  }}
				{{ Form::select('taskList_ids[]', $taskLists, 1, ['multiple'=>true])  }}

				<input type="submit" value="submit" />
				</form>

			</div><!-- END .col-wide -->
		</div><!-- END .row -->

<!-- end app/views/taskList.blade.php -->
@stop
