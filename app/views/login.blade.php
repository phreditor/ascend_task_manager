@extends('layouts.master')
@section('js')
{{$js or ''}}
<script src="/global/vendor/jquery.validate.min.js"></script>
<script src="/global/vendor/placeholders.jquery.min.js"></script>
<script>
	$('#loginform').validate();

	$('#user_id_select').on('change', function() {
		$('#user_id').val($('#user_id_select').val());
	});
</script>
@stop

@section('content')

	<div>
		<div class="row">
			<div class="column col-wide">
				<div class="page-header">
					<h1>Sign In</h1>
				</div><!-- END .page-header -->
			</div><!-- END .col-wide -->
		</div><!-- END .row -->

		<div class="row">
			<div class="column col-1-2 aligncenter">
        <form id="loginform" action="/login" method="post">
					<?php echo Form::token(); ?>
					@if (isset($errors) && count($errors) != 0)
						<div class="errors-container">
							<?php $messages = $errors->getMessages(); ?>

							<div class="errors">
							<ul>
								@foreach($messages as $error)
									<li>{{ $error[0] }}</li>
								@endforeach
							</ul>
							</div>
						</div>
					@endif
					<div class="row">
						<label for="username">Username</label>
						<input id="username" name="username" type="email" value="{{Input::old('username')}}" placeholder="name@email.com" required>
					</div>
					<div class="row">
						<label for="password">Password</label>
						<input id="password" name="password" type="password" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;" required>
						<a class="input-help" href="/password/getRemind">Forgot Password?</a>
					</div>
					<div class="row">
						<button class="btn" type="submit">Sign in</button>
					</div>
				</form>
			</div>
		</div><!-- END .row -->
<!-- end app/views/login.blade.php -->
@stop
