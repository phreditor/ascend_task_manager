<!DOCTYPE html>
<html class="no-js" lang="{{Config::get('app.locale', 'en')}}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{$title or ""}}{{BASE_TITLE}} - {{Config::get('app.language', '')}}</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/global/css/normalize.css">
	<link rel="stylesheet" href="/global/css/helper.css">
	<link rel="stylesheet" href="/global/css/main.css{{$dCache}}">
	<link rel="stylesheet" href="/global/css/header.css{{$dCache}}">
	<link rel="stylesheet" href="/global/css/footer.css{{$dCache}}">
	@yield('css')

	<script src="/global/vendor/modernizr-2.8.3.js"></script>
	<script>
		if (!Modernizr.mq('only all')) {
			var root = document.getElementsByTagName( 'html' )[0];
			root.className += ' no-mq';
		}
	</script>

	<style>
	textarea.echo { font-size: 10px; height: 40px; width: 300px;}
	</style>
</head>

<?php
	if(!isset($bodyClass)) $bodyClass='';

	if( null !== \Session::get('first_name') ) $bodyClass .= " authenticated";
?>

<body class="{{$bodyClass or ''}}"><!-- add class 'authenticated' when logged in -->

	<div class="content-container">

	@include('common.header')

	@yield('content')

	@include('common.footer')
	</div> <!-- END .content-container -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/global/vendor/jquery/jquery-1.11.3.min.js"><\/script>')</script>
	<script src="/global/js/main.js"></script>
	@yield('js')

</body>
</html>
