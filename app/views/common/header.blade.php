		<div class="masthead-container clearfix">
			<div class="account-box"><!-- authenticated .account-box -->

			@if( null !== \Session::get('first_name') )
				<p>Welcome, {{\Session::get('first_name')}} {{\Session::get('last_name')}}
				 </p>
				<p><a href="/logout">Log out </a>
				</p>

			@else
				<p><a href="/login">Log in</a></p>
			@endif

			</div>
		</div><!-- END .masthead-container -->
