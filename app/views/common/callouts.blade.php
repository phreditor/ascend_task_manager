		<div id="had-tab-callouts" class="callouts-container had-callouts selected">
			<div class="row">
			@foreach($content_boxes as $k => $v)
				@if($v->audience_id != 2)
				<div class="column col-1-3 callout">
					<h3 class="callout-headline">{{$v->box_headline}}</h3>
					<div class="callout-image">
						<img src="/uploads/{{$v->language_id}}/content/{{$v->image_file}}">
					</div>
					<div class="callout-body">
						{{$v->box_copy}}

						@if($v->page_headline != '' )
							<a class="page-link" href="/read_more/{{$v->slug}}">Read More</a>

						@elseif($v->read_more_link != '')

							@if(isset(parse_url($v->read_more_link)['host']) && parse_url($v->read_more_link)['host'] != parse_url(\Request::url())['host'])
								<a class="page-link" href="{{$v->read_more_link}}" target="_blank">Read More</a>
							@else
								<a class="page-link" href="{{$v->read_more_link}}">Read More</a>
							@endif

						@endif
					</div>
				</div>
				@endif
			@endforeach
			</div>
		</div><!-- END .callouts-container -->


		<div id="lyric-tab-callouts" class="callouts-container lyric-callouts">
			<div class="row">
			@foreach($content_boxes as $k => $v)
				@if($v->audience_id == 2)
				<div class="column col-1-3 callout">
					<h3 class="callout-headline">{{$v->box_headline}}</h3>
					<div class="callout-image">
						<img src="/uploads/{{$v->language_id}}/content/{{$v->image_file}}">
					</div>
					<div class="callout-body">
						{{$v->box_copy}}

						@if($v->page_headline != '' )
							<a class="page-link" href="/read_more/{{$v->slug}}">Read More</a>

						@elseif($v->read_more_link != '')

							@if(isset(parse_url($v->read_more_link)['host']) && parse_url($v->read_more_link)['host'] != parse_url(\Request::url())['host'])
								<a class="page-link" href="{{$v->read_more_link}}" target="_blank">Read More</a>
							@else
								<a class="page-link" href="{{$v->read_more_link}}">Read More</a>
							@endif

						@endif
					</div>
				</div>
				@endif
			@endforeach
			</div>
		</div><!-- END .callouts-container -->
