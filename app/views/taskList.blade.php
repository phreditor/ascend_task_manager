@extends('layouts.master')
@section('js')
{{$js or ''}}
<script src="/global/vendor/jquery.validate.min.js"></script>
<script src="/global/vendor/placeholders.jquery.min.js"></script>
@stop

@section('content')

		<div class="row">
			<div class="column col-wide">
				<div class="page-header">
					<h1>Task List</h1><br><br>
				</div><!-- END .page-header -->
<a href="tasklist/edit">Add Task List</a><br><br>
					<table style="width:800px;">
					@if(isset($taskLists) && $taskLists->count())
						<thead>
						<tr style="border-bottom:1px solid black; margin-bottom:3px;">
						<td>Action</td>
						<td>Name</td>
						<td># of Tasks</td>
						<td>Created</td>
						</tr>
						</thead>
						<tbody>
						@foreach($taskLists as $taskList)
							<tr>
							<td><a href="tasks/{{$taskList->id}}">Show</a> | <a href="tasklist/edit/{{$taskList->id}}">Edit</a></td>
							<td>{{$taskList->name}}</td>
							<td>{{$taskList->tasks()->count()}}</td>
							<td>{{$taskList->created_at}}</td>
							</tr>
						@endforeach
						</tbody>
					@else
						No tasks lists available for this user
					@endif

					</table>




			</div><!-- END .col-wide -->
		</div><!-- END .row -->

<!-- end app/views/taskList.blade.php -->
@stop
