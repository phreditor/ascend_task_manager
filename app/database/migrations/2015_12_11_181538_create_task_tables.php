<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('status_id')->unsigned();
			$table->string('description', 48)->default('');
			$table->timestamps();

			$table->index('status_id');
		});

		Schema::create('taskLists', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->string('name', 48)->default('');
			$table->timestamps();

			$table->index('user_id');
		});

		Schema::create('taskLists_tasks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('taskLists_id')->unsigned();
			$table->integer('tasks_id')->unsigned();
			$table->timestamps();

			$table->index('taskLists_id');
			$table->index('tasks_id');
		});

		Schema::create('list_taskStatus', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('status', 48)->default('');

			$table->index('status');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    DB::statement('SET FOREIGN_KEY_CHECKS = 0');

		$tables=['tasks', 'taskLists', 'taskLists_tasks', 'list_taskStatus'];

		foreach($tables as $t) {
			if (Schema::hasTable($t)) { Schema::drop($t); }
		}

		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
