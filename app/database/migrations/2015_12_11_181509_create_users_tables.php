<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('first_name', 64, 0)->default('');
			$table->string('last_name', 64, 0)->default('');
			$table->string('email', 255)->default('');
			$table->string('username', 255)->default('');
			$table->string('password', 128)->default('');

			$table->tinyInteger('active')->default(1);
			$table->string("remember_token")->nullable();
			$table->integer('logins')->default(0);
			$table->datetime('last_login')->default('0000-00-00 00:00:00');
			$table->timestamps();

			$table->unique('username');
			$table->index('email');
			$table->index('first_name');
			$table->index('last_name');
			$table->index('active');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    DB::statement('SET FOREIGN_KEY_CHECKS = 0');

		$tables=['users'];

		foreach($tables as $t) {
			if (Schema::hasTable($t)) { Schema::drop($t); }
		}

		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}
}
