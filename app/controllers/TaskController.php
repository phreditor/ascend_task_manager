<?php

class TaskController extends BaseController {

	public function getTaskList() {

		$user_id = \Session::get('user_id', 0);
		$taskLists = \TaskList::where('user_id', '=', $user_id)->get();

		return View::make('taskList', ['taskLists'=>$taskLists]);
	}


	public function getTaskListEdit($id = 0) {

		$taskList = \TaskList::find($id);

		return View::make('taskList_edit', ['taskList'=>$taskList]);
	}


	public function taskListProcess() {

		$input = \Input::all();

		if($input['id']) {
			$tl = TaskList::find($input['id']);
			$tl->name = $input['name'];
			$tl->save();
			return Redirect::route("tasklists");

		} else {
			$tl = new TaskList;
			$tl->name = $input['name'];
			$tl->user_id = \Session::get('user_id', 0);
			$tl->save();
			return Redirect::route("tasklists");

		}
		$taskList = \TaskList::find($tlist_id);

		return View::make('taskList_edit', ['taskList'=>$taskList]);
	}

	public function getTasks($taskList_id = 0) {

		$taskList = \TaskList::find($taskList_id);
		\Log::info(get_class()." taskList: ", array(print_r($taskList,true)));


		$tasks = $taskList->tasks;

\Log::info(get_class()." tasks: ", array(print_r($tasks,true)));

		return View::make('tasks', ['tasks'=>$tasks]);
	}


	public function getTaskEdit($id = 0) {

		$user_id = \Session::get('user_id', 0);
		$taskLists = \TaskList::where('user_id', '=', $user_id)->lists('name','id');

		$task = \Task::find($id);
		$taskStatuses = \TaskStatus::lists('status','id');

		return View::make('task_edit', ['task'=>$task, 'taskLists'=>$taskLists, 'taskStatuses'=>$taskStatuses]);
	}

	public function taskProcess() {

		$input = \Input::all();
		\Log::info(get_class()." input: ", array(print_r($input,true)));

		if($input['id']) {
			$tl = Task::find($input['id']);
			$tl->description = $input['description'];
			$tl->status_id = $input['status_id'];
			$tl->save();

			$existingLists = TaskList_Task::where('tasks_id', '=',$tl->id)->lists('id');
			\Log::info(get_class()." existingLists: ", array(print_r($existingLists,true)));

			foreach($input['taskList_ids'] as $taskList_id) {
				\Log::info(get_class()." taskList_id: ", array(print_r($taskList_id,true)));

				$tl_t = new TaskList_Task;
				$tl_t->tasks_id = $tl->id;
				$tl_t->taskLists_id = $taskList_id;
				$tl_t->save();
			}

			return Redirect::route("tasklists");

		} else {
			$tl = new Task;
			$tl->description = $input['description'];
			$tl->status_id = $input['status_id'];
			$tl->save();

			foreach($input['taskList_ids'] as $taskList_id) {
				\Log::info(get_class()." taskList_id: ", array(print_r($taskList_id,true)));
				$tl_t = new TaskList_Task;
				$tl_t->tasks_id = $tl->id;
				$tl_t->taskLists_id = $taskList_id;
				$tl_t->save();
			}
			return Redirect::route("tasklists");

		}

		$task = \Task::find($tlist_id);

		return View::make('taskList_edit', ['taskList'=>$taskList]);
	}

}
