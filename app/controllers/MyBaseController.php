<?php
use \CamRock\CRIfunctions as CRIfn;

class MyBaseController extends BaseController {

	public $title = '';
	public $visibilityPath = '';
	public $uploadPath = '';
	public $dCache = '';
	public $logMemo;
	public $logUser_id;

	public function __construct() {
		//Make certain key info available to all controllers and views
		View::share('MD5_PAD_PRE',MD5_PAD_PRE);
		View::share('MD5_PAD_POST',MD5_PAD_POST);
		View::share('dCache',DECACHE);

		$dCache = DECACHE;

	}

	public function makeMd5Id($id = 0) {
		$prfx = MD5_PAD_PRE;
		$sfx = MD5_PAD_POST;

		return md5($prfx.$id.$sfx);
	}

	public function getIdByMd5Id($md5ID, $table, $idName = 'id') {
		$prfx = MD5_PAD_PRE;
		$sfx = MD5_PAD_POST;

		$sql = "
		SELECT $idName FROM $table
		WHERE md5(CONCAT('$prfx',$idName,'$sfx')) = ?";
		$result = DB::select($sql, array($md5ID));

		if ($result) {
			return $result[0]->id;
		}

		return 0;
	}

	protected function logout() {
		Auth::logout();
		Session::flush();
	}

	public function isPostRequest() {
		return Input::server("REQUEST_METHOD") == "POST";
	}

	public function cleanInputs(&$i) {
		// Trims all non-array inputs
		foreach($i as $k => $v) {
			if(!is_array($v)) {
				$v=trim($v);
			}
		}
	}
}
