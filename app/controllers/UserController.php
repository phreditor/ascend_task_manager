<?php
use \CamRock\CRIfunctions as CRIfn;

class UserController extends MyBaseController
{
	function __construct() {
		parent::__construct();
	}

  public function login() {

  	// If not post rquest, return login form
    if ( ! $this->isPostRequest() ) {

		return View::make('login')->with(array(
			'title' => 'Sign In',
			'hdrText' => 'Sign In',
			'bodyClass' => 'login'
		));

  	// If is post request, then process login form and return result
    } else {
		return $this->processLogin();
    }
  }

	protected function processLogin() {
      $validator = $this->getLoginValidator();


      if ($validator->passes()) {
		$credentials = [
			"username" => trim(Input::get("username")),
			"password" => trim(Input::get("password"))
		];

        if (Auth::attempt($credentials)) {

			$user = User::where('username', '=', $credentials['username'])->first();

			Session::put('user_id', $user->id);
			Session::put('first_name', $user->first_name);
			Session::put('last_name', $user->last_name);
			Session::put('email', $user->email);

        } else {
			return $this->signInFailed("<strong>Credentials invalid.</strong>&nbsp; Did you <a href='/password/getRemind'>forget your password</a>?");
        }

		return Redirect::route("tasklists");

      } else {

      return Redirect::back()
        ->withInput()
        ->withErrors($validator);
      }

	}

	public function logout() {

		Auth::logout();
		Session::flush();
		return Redirect::route('login');
	}

  protected function getLoginValidator() {
    return Validator::make(Input::all(), [
      "username" => "required",
      "password" => "required"
    ]);
  }

  protected function signInFailed($msg) {
	return Redirect::back()->withErrors([
	  "password" => ["<div class='login-error'>$msg</div>"]
	]);
  }

}
